/*var x = {
    '1. open': '19028.3594',
    '2. high': '19121.0098',
    '3. low': '18213.6504',
    '4. close': '18591.9297',
    '5. volume': '787970000'
  }

console.log(x["5. volume"])*/
let fs = require('fs');

/* No exemplo abaixo, informamos o local que será criado o arquivo
toda a informação que esse arquivo conterá, e por ultimo temos nossa função callback */
fs.writeFile("./example.txt",'Um breve texto aqui!', function(err){
	//Caro ocorra algum erro
  if(err){
		return console.log('erro')
	}
  //Caso não tenha erro, retornaremos a mensagem de sucesso
	console.log('Arquivo Criado');
});