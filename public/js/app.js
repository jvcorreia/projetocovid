//Não é necessário transformar o datas em OBJ pois ele já está sendo passado
// como OBJ para a página

// colocando os dados no grafico 
var obj =  JSON.parse(datas);
var ctx = document.getElementById('myChart').getContext("2d");

var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels:obj[0],
        datasets: [{     
            label: 'Casos de Covid-19',
            data: obj[1],
            borderWidth: 6,
            borderColor:"rgba(0,255,0,0.85)",
            backgroundColor: "transparent",
           
            
        },{
            label: 'Mortes pela Covid-19',
            data: obj[2],
            borderWidth: 6,
            borderColor:"rgba(255,0,0,0.85)",
            backgroundColor:"transparent",
           
        },{
            label: 'Recuperados da Covid-19',
        
            data: obj[3],
            borderWidth: 6,
            borderColor:"rgba(0,0,205,0.85)",
            backgroundColor:"transparent",
             
        },{
            label: 'Fechamento da bolsa *1000',
            data: obj[4] ,
            borderWidth: 6,
            borderColor:"rgba(255,150,0,0.85)",
            backgroundColor:"transparent",
            type: 'bar'
            
            
        },{
            label: 'Volume da bolsa  /100',
            data: obj[5],
            borderWidth: 6,
            borderColor:"rgba(0,0,0,0.85)",
            backgroundColor:"transparent",
            type: "bar"
            
        }]
    },
    options: {
        
        tooltips: {
            mode: 'index'
        },
        title: {
            display: true,
            text: 'Dados Covid-19 x Dados Bolsa de Valores'
        },
        scales: {
            yAxes:[{
                distribution: 'series',
        
                
            }],
            xAxes: [{
                offset : true,
                type: 'time',
                distribution: 'series',
               
             }]
         }
    }
}) 

function Funçao_pdf(){

    var element = document.getElementById('teste')
    html2canvas(element).then((canvas)=>{
        var imgData = canvas.toDataURL("image/jpeg")
        var doc = new jsPDF()
        var imgHeight = canvas.height * 208 / canvas.width;
        doc.addImage(imgData, 'JPEG', 0,0, 208, imgHeight)
        doc.setProperties ({
            title: 'Análise do Covid no contexto Econômico',
        });
        doc.save('Análise do Covid no contexto Econômico.pdf');

    })

}