
const { NovelCovid } = require('novelcovid');//Api Covd
const http = require('http');

const readline = require('readline');
 
const track = new NovelCovid();

const chavealpha = 'S41GDLHL4C6MJSHN' //Chave do Alpha Vantage API
const alpha = require('alphavantage')({ key:chavealpha}); //Inicialização

let fs = require('fs');

var express = require("express");

function delay(milliseconds) { //Função para delay
    const date = Date.now();
    let currentDate = null;
    do {
      currentDate = Date.now();
    } while (currentDate - date < milliseconds);
  }

//(BolsaAPI)
async function pegandodadosbolsa(indicemercado) { //Função para pegar os dados da bolsa e retornar o JSON
    let retorno = alpha.data.daily(indicemercado);
    return retorno;
}
   
//(CoronaAPI)
async function pegandodadoscorona(pais) { //Função para pegar os dados do corona e retornar o JSON
    let retorno = track.historical(null,pais);
    return retorno;
}

var listaMetaDados = [ [], ['Brazil', 'Brasil','VALE', 'Vale S.A'],
                           ['Brazil', 'Brasil','BDORY', 'Banco do Brasil'],
                           ['Brazil', 'Brasil',"PBR", 'Petrobrás'],
                           ['USA', 'Estados Unidos', "IBM", 'IBM'],
                           ['USA', 'Estados Unidos', "msft", 'Microsoft'],
                           ['USA', 'Estados Unidos', "AAPL", 'Apple'],
                           ['China', 'China', "BABA", "Alibaba Group"],
                           ['China', 'China', "BACHF", "Banco da China"],
                           ['China', 'China', "XIACF", "Xiaomi Corporation"],
                           ['Italy', 'Itália', "FCAU", "Fiat"],
                           ['Italy', 'Itália', "RACE", "Ferrari"],
                           ['Italy', 'Itália', "XIACF", "Xiaomi Corporation"],
                      ]

//Acessa os dados e os trata
async function jsonCovidBolsa(pais, indicemercado, lista) {

    /* Inicio tratamento dados covid*/

    //Recebendo o JSON do covid
    var covidJson = (await pegandodadoscorona(pais));

    //Acessando os dados
    var acessoCovidCasos = covidJson.timeline.cases;
    var acessoCovidMortes = covidJson.timeline.deaths;
    var acessoCovidRecuperados = covidJson.timeline.recovered;

    //Atribuindo os valores para serem organizados mais abaixo
    var datasCorona = Object.keys(acessoCovidCasos);
    var covidCasos = Object.values(acessoCovidCasos);
    var covidMortes = Object.values(acessoCovidMortes);
    var covidRecuperados = Object.values(acessoCovidRecuperados);
    var datasFormatadas = [];

    //Formatação de datas para igualar ao modelo da bolsa
    for (i = 0; i < datasCorona.length; i++) {
        var dataI = new Date(datasCorona[i]);
        //Deixando no modelo yyyy-mm-dd e colocando os zeros onde for preciso
        if ((dataI.getMonth() + 1) < 10 && dataI.getDate() < 10) {
            datasFormatadas.push('2020-0' + (dataI.getMonth() + 1) + '-0' + dataI.getDate())
        }
        else if ((dataI.getMonth() + 1) < 10 && dataI.getDate() >= 10) {
            datasFormatadas.push('2020-0' + (dataI.getMonth() + 1) + '-' + dataI.getDate())
        }
        else if ((dataI.getMonth() + 1) >= 10 && dataI.getDate() < 10) {
            datasFormatadas.push('2020-' + (dataI.getMonth() + 1) + '-0' + dataI.getDate())
        }
        else {
            datasFormatadas.push('2020-' + (dataI.getMonth() + 1) + '-' + dataI.getDate())
        }
    }
    //console.log(datasFormatadas);
    /* Fim tratamento dados covid*/


    /* Inicio tratamento dados bolsa*/

    console.log('Estamos preparando os dados.....')
    var bolsaJson = (await pegandodadosbolsa(indicemercado));
    var bolsaJson2 = bolsaJson["Time Series (Daily)"];
    //console.log(bolsaJson)
    var listaFinalFormatada = [];//'5. volume'

    var datanahora = '';

    var dataArray = [];
    var casosArray = [];
    var mortesArray = [];
    var recuperadosArray = [];
    var fechamentoBolsaArray = [];
    var volumeBolsaArray = [];
    var metadadosERelatorio = []; // [0] pais, [1] empresa, [2] relatorio


    
    for (i = 0; i < datasFormatadas.length; i++) {
        datanahora = (datasFormatadas[i].toString())
        var objfinal = (bolsaJson2[datanahora]) //Vai verificar os dados no dia
        if (objfinal != undefined) { //Ele só cria se existir ambos os dados no dia
            dataArray.push(datasCorona[i])
            casosArray.push(covidCasos[i])
            mortesArray.push(covidMortes[i])
            recuperadosArray.push(covidRecuperados[i])
            fechamentoBolsaArray.push(objfinal["4. close"]*1000)
            volumeBolsaArray.push(objfinal["5. volume"]/100)
        }
    }

    var paisRelatorio = lista[1];
    var empresaRelatorio = lista[3];
    var maiorVolumeBolsa = Math.max.apply(null, volumeBolsaArray );
    var menorVolumeBolsa = Math.min.apply(null, volumeBolsaArray );
    var amplitudeVolume = maiorVolumeBolsa - menorVolumeBolsa;
    var indexMenorVolume = volumeBolsaArray.indexOf(menorVolumeBolsa);
    var dataMenorVolume = dataArray[indexMenorVolume];
    var maiorFechamentoBolsa = Math.max.apply(null, fechamentoBolsaArray );
    var menorFechamentoBolsa = Math.min.apply(null, fechamentoBolsaArray );
    var amplitudeFechamento = maiorFechamentoBolsa - menorFechamentoBolsa;
    var indexMaiorFechamento = fechamentoBolsaArray.indexOf(maiorFechamentoBolsa);
    var mortesEmMaiorFechamento = mortesArray[indexMaiorFechamento];


    var amplitudeVolume = parseFloat(amplitudeVolume.toFixed(2));
    let somaNumeros = 0
  for(let i in volumeBolsaArray) {
    somaNumeros += volumeBolsaArray[i];
  }
  let mediaNumero = somaNumeros/volumeBolsaArray.length;
  console.log(somaNumeros + ' ' + mediaNumero + ' ' + amplitudeVolume)

  if(amplitudeVolume >= mediaNumero){
      var resultadoImpacto = 'grande';
  }else if (amplitudeVolume >= mediaNumero*1.2){
    var resultadoImpacto = 'médio';
  }
  else{
    var resultadoImpacto = 'pequeno';
  }

  console.log(resultadoImpacto);
    metadadosERelatorio.push(lista[3],lista[1],amplitudeVolume,menorVolumeBolsa,
        dataMenorVolume,maiorFechamentoBolsa,mortesEmMaiorFechamento,resultadoImpacto);

    //Adicionanando as listas dentro da lista que será passada.
    listaFinalFormatada = [dataArray, casosArray, mortesArray, recuperadosArray, fechamentoBolsaArray, volumeBolsaArray, metadadosERelatorio];

    fs.writeFile("./public/datas.json","datas ='" + JSON.stringify(listaFinalFormatada) + "'", function(err){
      if(err){
            return console.log('Não foi possível criar o arquivo.')
        }
        
    });
    
    
    //Vamos agora criar o servidor e passar a página principal e os dados prontos.
    
    var app = express(); //Acionando o Express
    
    app.use(express.static('public'));
    console.log('Gerando seu relatório.....');
    //Setando o JS e CSS
    app.use('/css', express.static(__dirname + '/css'));
    app.use('/js', express.static(__dirname + '/js'));
    
    
    var server = app.listen(3000, function(){
        var port = server.address().port;
        console.log("Server criado na porta 3000, clique aqui para ver o relatório-> http://localhost:%s", port);
    });


}

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
  });
  
  //Promise questão
  const question = (str) => new Promise(resolve => rl.question(str, resolve));
  
  //Menu que vai rodar aplicação

     async function menu () {
      console.log('----------------------------------------------------------------------------------');  
      console.log('Bem vindo ao menu do programa que calcula o impacto do covid de acordo com o país.');
      console.log('----------------------------------------------------------------------------------');
      
      console.log('Escolha o número da empresa para ver a análise:\n-Brasileiras: 1-Vale  2-Banco do Brasil  3-Petrobrás ');
      console.log('-Americanas:  4-IBM  5-Microsoft  6-Apple ');
      console.log('-Chinesas:    7-Alibaba Group  8-Banco da China  9-Xiaomi Corporation ');
      console.log('-Italianas:   10-Fiat  11-Ferrari');

      const resposta = await question("");
      
      if (resposta > 0 && resposta < 12) {  
        jsonCovidBolsa(listaMetaDados[parseInt(resposta)][0], listaMetaDados[parseInt(resposta)][2],listaMetaDados[parseInt(resposta)]);
      }
      else{
        console.log('O número do país não existe.')
      }
      
      rl.close();
  };
  
//Inicia o menu
menu();



